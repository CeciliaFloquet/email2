// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import * as Twilio from 'twilio';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
    
    console.log( result );
    
    //test
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken =process.env.TWILIO_AUTH_TOKEN;
    const client = require('twilio')(accountSid, authToken);
    const twilioNumber= process.env.TWILIO_PHONE_NUMBER;
   
    const phoneNumbers = [ '+16045181850'];   
    function sendText(){
                    
        client.messages.create({
            body: result.msg,
            to: phoneNumbers,
            from: twilioNumber
        })
            
    }
    
    if ( !phoneNumbers.includes(result['to'])) {
        console.log( "BAD: number " + result['to']);
        return context;
    } else{
        console.log( "GOOD: number " + result['to']);
        sendText();
    }

    return context;
  };
}
